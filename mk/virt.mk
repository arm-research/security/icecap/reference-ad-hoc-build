sel4_kernel_platform := qemu-arm-virt
host_linux_src := $(deps)/linux@icecap
host_uboot_src := $(deps)/u-boot@icecap-host
host_uboot_boot_cmd := smhload ./system/load-host.script.uimg 0x80000000; source 0x80000000

system := $(disposable_dir)/system

sel4_dts_path := $(sel4_src)/tools/dts/virt.dts


.PHONY: build
build: elfloader system-dir

.PHONY: clean-plat
clean-plat:
	rm -f $(sel4_dts_path) system

.PHONY: run
run:
	ln -sf $(system) system
	qemu-system-aarch64 \
		-machine virt,virtualization=on,gic-version=2 -cpu cortex-a57 -smp 4 -m 3072 \
		-nographic \
		-semihosting-config enable=on,target=native \
		-device virtio-net-device,netdev=netdev0 \
		-device virtio-9p-device,mount_tag=share,fsdev=share \
		-serial mon:stdio \
		-netdev user,id=netdev0 \
		-fsdev local,id=share,security_model=none,readonly,path=./system/host-userspace \
		-kernel $(elfloader)


sel4-configure: $(sel4_dts_path)

$(sel4_dts_path): $(misc_build_dir)/virt.dtb
	dtc -I dtb -O dts -o $@ $<

$(misc_build_dir)/virt.dtb: | $(misc_build_dir)
	qemu-system-aarch64 \
		-machine virt,virtualization=on,dumpdtb=$@,gic-version=2 -cpu cortex-a57 -smp 4 -m 3072 \
		-nographic \
		-semihosting-config enable=on,target=native \
		-device virtio-net-device,netdev=netdev0 \
		-device virtio-9p-device,mount_tag=share,fsdev=share \
		-netdev user,id=netdev0 \
		-fsdev local,id=share,security_model=none,readonly,path=.


system_files := \
	load-host.script.uimg \
	Image \
	host.dtb \
	initramfs \
	host-userspace/spec.bin \
	host-userspace/bin/icecap-host \
	host-userspace/bin/host-test.sh

system_files_with_prefix := $(addprefix $(system)/,$(system_files))

.PHONY: system-dir
system-dir: $(system_files_with_prefix)

$(system)/load-host.script.uimg: $(misc_build_dir)/load-host.script.uimg
$(system)/Image: $(host_linux_build_dir)/$(linux_image_path)
$(system)/host.dtb: $(icedl_firmware_dir)/cdl/host_vm.dtb
$(system)/initramfs: $(call buildroot_rootfs_for,host)
$(system)/host-userspace/spec.bin: $(icedl_realm_dir)/spec.bin
$(system)/host-userspace/bin/icecap-host: $(rust_musl_bins)/icecap-host
$(system)/host-userspace/bin/host-test.sh: $(src)/host-test.sh

$(system_files_with_prefix):
	install -D -T $< $@
