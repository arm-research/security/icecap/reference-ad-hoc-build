sel4_kernel_platform := bcm2711
host_linux_src := $(deps)/linux@icecap-rpi4
host_uboot_src := $(deps)/u-boot@icecap-host
host_uboot_boot_cmd := \
	load mmc 0:1 0x10070000 payload/load-host.script.uimg; source 0x10070000

firmware_uboot_src := $(deps)/u-boot@icecap
firmware_uboot_build_dir := $(sticky_dir)/u-boot/firmware
firmware_uboot_boot_cmd := \
	load mmc 0:1 0x100000 load-icecap.script.uimg; source 0x100000

boot := $(disposable_dir)/boot
boot_external := $(misc_build_dir)/boot

sel4_dts_path := $(sel4_src)/tools/dts/rpi4.dts


.PHONY: build
build: boot-dir

.PHONY: clean-plat
clean-plat:
	rm -f $(sel4_dts_path)


sel4-configure: $(sel4_dts_path)

$(sel4_dts_path): $(boot_external)
	dtc -I dtb -O dts -o $@ $</bcm2711-rpi-4-b.dtb


$(firmware_uboot_build_dir):
	mkdir -p $@

$(firmware_uboot_build_dir)/.config:
	$(MAKE) \
		-C $(firmware_uboot_src) \
		O=$(abspath $(firmware_uboot_build_dir)) \
		ARCH=arm64 \
		CROSS_COMPILE=aarch64-linux-gnu- \
		rpi_4_defconfig
	sed -i 's|BOOTDELAY=2|BOOTDELAY=0|' $@
	sed -i 's|BOOTCOMMAND="run distro_bootcmd"|BOOTCOMMAND="$(firmware_uboot_boot_cmd)"|' $@

$(firmware_uboot_build_dir)/u-boot.bin: $(firmware_uboot_build_dir)/.config 
	$(MAKE) \
		-j$$(nproc) \
		-C $(firmware_uboot_src) \
		O=$(abspath $(firmware_uboot_build_dir)) \
		CROSS_COMPILE=aarch64-linux-gnu- \
		u-boot.bin


rpi4_firmware_url := https://github.com/raspberrypi/firmware.git
rpi_firmware_tag := 1.20211118

$(boot_external): | $(misc_build_dir)
	rm -rf $@
	svn export $(rpi4_firmware_url)/tags/$(rpi_firmware_tag)/boot $@


boot_files := \
	config.txt \
	kernel8.img \
	load-icecap.script.uimg \
	icecap.elf \
	payload/load-host.script.uimg \
	payload/Image \
	payload/host.dtb \
	payload/initramfs \
	bin/icecap-host \
	bin/host-test.sh \
	spec.bin

boot_files_with_prefix := $(addprefix $(boot)/,$(boot_files))

.PHONY:
boot-dir: $(addprefix $(boot)/,$(boot_files))
	cp -R -u $(boot_external)/overlays $(boot)/overlays
	cp -R -u \
		$(boot_external)/bootcode.bin \
		$(boot_external)/*.dtb \
		$(boot_external)/start*.elf \
		$(boot_external)/fixup*.dat \
		$(boot)

$(boot)/config.txt: $(src)/$(icecap_plat)/config.txt
$(boot)/kernel8.img: $(firmware_uboot_build_dir)/u-boot.bin
$(boot)/load-icecap.script.uimg: $(misc_build_dir)/load-icecap.script.uimg
$(boot)/icecap.elf: $(elfloader_build_dir)/build/elfloader
$(boot)/payload/load-host.script.uimg: $(misc_build_dir)/load-host.script.uimg
$(boot)/payload/Image: $(host_linux_build_dir)/$(linux_image_path)
$(boot)/payload/host.dtb: $(icedl_firmware_dir)/cdl/host_vm.dtb
$(boot)/payload/initramfs: $(call buildroot_rootfs_for,host)
$(boot)/spec.bin: $(icedl_realm_dir)/spec.bin
$(boot)/bin/icecap-host: $(rust_musl_bins)/icecap-host
$(boot)/bin/host-test.sh: $(src)/host-test.sh

$(boot_files_with_prefix):
	install -D -T $< $@
