PLAT ?= virt

icecap_plat := $(PLAT)

icecap := icecap
deps := deps
src := src
build := build

icecap_src := icecap/src
c_src := $(icecap_src)/c
rust_src := $(icecap_src)/rust
python_src := $(icecap_src)/python
support_src := $(icecap_src)/support

sel4_src := $(deps)/seL4
sel4_tools_src := $(deps)/seL4_tools
elfloader_src := $(sel4_tools_src)/elfloader-tool
capdl_src := $(deps)/capdl
realm_linux_src := $(deps)/linux@icecap
buildroot_src := $(deps)/buildroot

sticky_dir := $(build)/$(icecap_plat)/sticky
disposable_dir := $(build)/$(icecap_plat)/disposable

rust_build_dir := $(sticky_dir)/target
host_uboot_build_dir := $(sticky_dir)/u-boot/host
host_linux_build_dir := $(sticky_dir)/linux/host
realm_linux_build_dir := $(sticky_dir)/linux/realm
host_buildroot_build_dir := $(sticky_dir)/buildroot/host
realm_buildroot_build_dir := $(sticky_dir)/buildroot/realm

c_build_dir := $(disposable_dir)/c
sel4_build_dir := $(disposable_dir)/cmake/sel4
elfloader_build_dir := $(disposable_dir)/cmake/elfloader
icedl_build_dir := $(disposable_dir)/icedl
misc_build_dir := $(disposable_dir)/misc

sel4_include_dir := $(sel4_build_dir)/install/libsel4/include
sel4_lib_dir := $(sel4_build_dir)/install/lib

platform_info_h := $(misc_build_dir)/platform_info.h
object_sizes_yaml := $(misc_build_dir)/object_sizes.yaml

rust_icecap_bins := $(rust_build_dir)/aarch64-icecap/release
rust_musl_bins := $(rust_build_dir)/aarch64-unknown-linux-musl/release
rust_dev_bins := $(rust_build_dir)/release

capdl_tool := $(capdl_src)/capDL-tool/parse-capDL

icedl_component_dir := $(icedl_build_dir)/components
icedl_firmware_dir := $(icedl_build_dir)/firmware
icedl_firmware_cdl := $(icedl_firmware_dir)/cdl/icecap.cdl
icedl_realm_dir := $(icedl_build_dir)/realm
icedl_realm_cdl := $(icedl_realm_dir)/cdl/icecap.cdl

capdl_loader := $(c_build_dir)/capdl-loader/install/bin/capdl-loader.elf
app_elf := $(misc_build_dir)/app.elf
elfloader := $(elfloader_build_dir)/build/elfloader

linux_image_path := arch/arm64/boot/Image

buildroot_plat_extension_for = $(if $(findstring host,$(1)),/$(icecap_plat),)
buildroot_build_dir_for = $($(1)_buildroot_build_dir)
buildroot_rootfs_for = $(buildroot_build_dir_for)/images/rootfs.cpio.gz
buildroot_defconfig_for = $(src)/buildroot/$(1)$(buildroot_plat_extension_for)/defconfig

compiler_some_libc_include := /usr/lib/gcc-cross/aarch64-linux-gnu/10/include/


# NOTE this file must define:
#  variables:
#   - sel4_kernel_platform
#   - host_linux_src
#   - host_uboot_src
#   - host_uboot_boot_cmd
#  rules:
#   - clean-plat
include mk/$(icecap_plat).mk


# default to building nothing
.PHONY: none
none:

.PHONY: clean
clean: clean-plat
	rm -rf $(disposable_dir)

.PHONY: deep-clean
deep-clean: clean
	rm -rf $(sticky_dir)

.PHONY: very-deep-clean
very-deep-clean: deep-clean
	rm -rf $(build)


mk_dirs := \
	$(sel4_build_dir) $(elfloader_build_dir) \
	$(host_uboot_build_dir) $(host_linux_build_dir) $(realm_linux_build_dir) \
	$(host_buildroot_build_dir) $(realm_buildroot_build_dir) \
	$(misc_build_dir)

$(mk_dirs):
	mkdir -p $@


$(capdl_tool):
	$(MAKE) -C $(capdl_src)/capDL-tool


$(host_uboot_build_dir)/defconfig: $(support_src)/framework/host/$(icecap_plat)/u-boot.defconfig | $(host_uboot_build_dir)
	sed 's|BOOTCOMMAND="x"|BOOTCOMMAND="$(host_uboot_boot_cmd)"|' $< > $@

$(host_uboot_build_dir)/.config: $(host_uboot_build_dir)/defconfig
	$(MAKE) \
		-C $(host_uboot_src) \
		O=$(abspath $(host_uboot_build_dir)) \
		ARCH=arm64 \
		CROSS_COMPILE=aarch64-linux-gnu- \
		KCONFIG_ALLCONFIG=$(abspath $<) \
		alldefconfig

$(host_uboot_build_dir)/u-boot.bin: $(host_uboot_build_dir)/.config 
	$(MAKE) \
		-j$$(nproc) \
		-C $(host_uboot_src) \
		O=$(abspath $(host_uboot_build_dir)) \
		CROSS_COMPILE=aarch64-linux-gnu- \
		u-boot.bin


$(host_linux_build_dir)/.config: $(support_src)/framework/host/$(icecap_plat)/linux.defconfig
	$(MAKE) \
		-C $(host_linux_src) \
		O=$(abspath $(host_linux_build_dir)) \
		ARCH=arm64 \
		CROSS_COMPILE=aarch64-linux-gnu- \
		KCONFIG_ALLCONFIG=$(abspath $<) \
		alldefconfig

$(host_linux_build_dir)/$(linux_image_path): $(host_linux_build_dir)/.config
	$(MAKE) \
		-j$$(nproc) \
		-C $(host_linux_src) \
		O=$(abspath $(host_linux_build_dir)) \
		ARCH=arm64 \
		CROSS_COMPILE=aarch64-linux-gnu- \
		Image


$(realm_linux_build_dir)/.config: $(support_src)/framework/guest/minimal/linux.defconfig
	$(MAKE) \
		-C $(realm_linux_src) \
		O=$(abspath $(realm_linux_build_dir)) \
		ARCH=arm64 \
		CROSS_COMPILE=aarch64-linux-gnu- \
		KCONFIG_ALLCONFIG=$(abspath $<) \
		alldefconfig

$(realm_linux_build_dir)/$(linux_image_path): $(realm_linux_build_dir)/.config
	$(MAKE) \
		-j$$(nproc) \
		-C $(realm_linux_src) \
		O=$(abspath $(realm_linux_build_dir)) \
		ARCH=arm64 \
		CROSS_COMPILE=aarch64-linux-gnu- \
		Image


buildroot_make_head_for = \
	$(MAKE) -C $(buildroot_src) O=$(abspath $(buildroot_build_dir_for)) # BR2_JLEVEL=$$(nproc) crashes container

define buildroot_rules_for

$(buildroot_build_dir_for)/.config: $(buildroot_defconfig_for) | $(buildroot_build_dir_for)
	$(buildroot_make_head_for) \
		KCONFIG_ALLCONFIG=$$(abspath $$<) \
		alldefconfig
	sed -i 's,@rootfs_overlay@,$(abspath $(src)/buildroot/common/rootfs-overlay) $(abspath $(src)/buildroot/$(1)$(buildroot_plat_extension_for)/rootfs-overlay),' $$@
	sed -i 's,@post_build_script@,$(abspath $(src)/buildroot/common/post_build.sh) $(abspath $(src)/buildroot/$(1)$(buildroot_plat_extension_for)/post_build.sh),' $$@

$(buildroot_rootfs_for): $(buildroot_build_dir_for)/.config
	$(buildroot_make_head_for)

.PHONY: $(1)-buildroot-menuconfig
$(1)-buildroot-menuconfig: $(buildroot_build_dir_for)/.config
	$(buildroot_make_head_for) menuconfig
	$(buildroot_make_head_for) savedefconfig
	mv $(buildroot_build_dir_for)/defconfig $(buildroot_defconfig_for)

endef

$(eval $(call buildroot_rules_for,host))
$(eval $(call buildroot_rules_for,realm))


.PHONY: sel4-configure
sel4-configure: $(sel4_build_dir)
	cmake -G Ninja \
		-DCMAKE_TOOLCHAIN_FILE=$(abspath $(sel4_src)/gcc.cmake) \
		-DCROSS_COMPILER_PREFIX=aarch64-linux-gnu- \
		-DCMAKE_INSTALL_PREFIX=$(abspath $(sel4_build_dir)/install) \
		-DHACK_SEL4_SRC=$(abspath $(sel4_src)) \
		-DHACK_SEL4_KERNEL_PLATFORM=$(sel4_kernel_platform) \
		-C $(abspath $(src)/cmake-config/seL4.cmake) \
		-S $(abspath $(sel4_src)) \
		-B $(abspath $(sel4_build_dir)/build)

.PHONY: sel4-build
sel4-build: sel4-configure
	ninja -C $(abspath $(sel4_build_dir)/build) all kernel.elf sel4

$(sel4_build_dir)/build/libsel4/libsel4.a: sel4-build

$(sel4_build_dir)/install/lib/%: $(sel4_build_dir)/build/libsel4/%
	install -D -T $< $@

.PHONY: sel4-install
sel4-install: sel4-build $(sel4_build_dir)/install/lib/libsel4.a
	ninja -C $(abspath $(sel4_build_dir)/build) install

.PHONY: sel4
sel4: sel4-install


sel4_cmake_config_prefixes := Kernel LibSel4 HardwareDebugAPI

$(misc_build_dir)/kernel-config.txt: sel4 | $(misc_build_dir)
	sed -n 's,^\([A-Za-z0-9][^:]*\):\([^=]*\)=\(.*\)$$,\1:\2=\3,p' $(sel4_build_dir)/build/CMakeCache.txt \
		| grep -e '$$.^' $(addprefix -e ^,$(sel4_cmake_config_prefixes)) \
		| sort \
		> $@

$(misc_build_dir)/kernel-config.cmake: $(misc_build_dir)/kernel-config.txt
	sed 's/^\([^:]*\):\([^=]*\)=\(.*\)$$/set(\1 "\3" CACHE \2 "")/' $< \
		> $@


$(platform_info_h): sel4 | $(misc_build_dir)
	python3 $(sel4_tools_src)/cmake-tool/helpers/platform_sift.py --emit-c-syntax \
		$(sel4_build_dir)/build/gen_headers/plat/machine/platform_gen.yaml > $@

$(object_sizes_yaml): sel4 | $(misc_build_dir)
	aarch64-linux-gnu-gcc -E -P - -I$(sel4_include_dir) < $(capdl_src)/object_sizes/object_sizes.yaml > $@


elfloader_c:
	$(MAKE) -f $(c_src)/Makefile CROSS_COMPILE=aarch64-linux-gnu- \
		BUILD=$(c_build_dir)/for-elfloader/build OUT=$(c_build_dir)/for-elfloader/install \
		ROOTS=$(c_src)/boot/cpio/icecap.mk \
		install

.PHONY: userspace_c
userspace_c: sel4
	$(MAKE) -f $(c_src)/Makefile CROSS_COMPILE=aarch64-linux-gnu- \
		BUILD=$(c_build_dir)/for-userspace/build OUT=$(c_build_dir)/for-userspace/install \
		CFLAGS="-I$(sel4_include_dir)" \
		ICECAP_RUNTIME_CONFIG_IN=/dev/null \
		ROOTS=$(c_src)/icecap-runtime/icecap.mk \
		install


package_args_for = $(shell awk '{print "-p " $$$$0}' < $(rust_src)/support/crates-for-$(1).txt)
seL4_package_args = $(call package_args_for,seL4)
linux_package_args = $(call package_args_for,linux)

.PHONY: rust-dev
rust-dev:
	cargo build \
		-Z unstable-options \
		--locked \
		--release \
		--target-dir $(rust_build_dir) \
		--manifest-path $(rust_src)/Cargo.toml \
		$(linux_package_args)

.PHONY: rust-musl
rust-musl:
	cargo build \
		-Z unstable-options \
		--locked \
		--release \
		--target-dir $(rust_build_dir) \
		--target aarch64-unknown-linux-musl \
		--manifest-path $(rust_src)/Cargo.toml \
		$(linux_package_args)

icecap_rustflags := \
	--cfg=icecap_plat=\"$(icecap_plat)\" \
	-L$(abspath $(sel4_lib_dir)) \
	-L$(abspath $(c_build_dir)/for-userspace/install/lib)

.PHONY: rust-none
rust-none: userspace_c
	RUST_TARGET_PATH=$(abspath $(rust_src)/support/targets) \
	CARGO_TARGET_AARCH64_ICECAP_RUSTFLAGS="$(icecap_rustflags)" \
	BINDGEN_EXTRA_CLANG_ARGS="-I$(abspath $(sel4_include_dir))" \
		cargo build \
			-Z unstable-options \
			-Z build-std=core,alloc,compiler_builtins -Z build-std-features=compiler-builtins-mem \
			--locked \
			--release \
			--target-dir $(rust_build_dir) \
			--target aarch64-icecap \
			--manifest-path $(rust_src)/Cargo.toml \
			$(seL4_package_args)

$(rust_musl_bins)/icecap-host: rust-musl


$(misc_build_dir)/host.dtb: \
		$(sel4_src)/tools/dts/$(icecap_plat).dts \
		$(support_src)/hypervisor/host/common/host.dtsa \
		$(support_src)/hypervisor/host/$(icecap_plat)/host.dtsa \
		| $(misc_build_dir)
	cat $^ | dtc -I dts -O dtb -o $@

$(misc_build_dir)/realm.dtb: \
		$(support_src)/hypervisor/realm/device-tree/base.dts \
		$(support_src)/hypervisor/realm/device-tree/$(icecap_plat).dtsa \
		| $(misc_build_dir)
	cat $^ | dtc -I dts -O dtb -o $@


icedl_components := \
	hypervisor-idle \
	hypervisor-fault-handler \
	icecap-generic-timer-server \
	hypervisor-serial-server \
	hypervisor-event-server \
	hypervisor-benchmark-server \
	hypervisor-resource-server \
	hypervisor-host-vmm \
	hypervisor-realm-vmm

icedl_components_prepared := \
	$(foreach x, \
		min full, \
		$(foreach y,$(icedl_components),$(icedl_component_dir)/$(y).$(x).elf) \
	)

$(foreach x,$(icedl_components), \
	$(patsubst %,$(rust_icecap_bins)/%.elf,$(x))): rust-none

$(icedl_component_dir)/%.full.elf: $(rust_icecap_bins)/%.elf
	install -D -T $< $@

$(icedl_component_dir)/%.min.elf: $(icedl_component_dir)/%.full.elf
	aarch64-linux-gnu-strip -s $< -o $@

mk_component_image_entry = "image": { \
	"full": "$(abspath $(icedl_component_dir)/$(1).full.elf)", \
	"min": "$(abspath $(icedl_component_dir)/$(1).min.elf)" \
}

mk_simple_component_entry = "$(1)": { $(call mk_component_image_entry,hypervisor-$(subst _,-,$(1))) }

firmware_simple_components := \
	idle \
	fault_handler \
	serial_server \
	event_server \
	benchmark_server \
	host_vmm

firmware_json = { \
	"plat": "$(icecap_plat)", \
	"num_cores": 4, \
	"num_realms": 2, \
	"default_affinity": 1, \
	"hack_realm_affinity": 1, \
	"object_sizes": "$(abspath $(object_sizes_yaml))", \
	"components": { \
		$(foreach component,$(firmware_simple_components), \
			$(call mk_simple_component_entry,$(component)),) \
		"timer_server": { \
			$(call mk_component_image_entry,icecap-generic-timer-server) \
		}, \
		"resource_server": { \
			"heap_size": $(shell python3 -c 'print(128 * 2**20)'), \
			$(call mk_component_image_entry,hypervisor-resource-server) \
		}, \
		"host_vm": { \
			"dtb": "$(abspath $(misc_build_dir)/host.dtb)", \
			"kernel": "$(abspath $(host_uboot_build_dir)/u-boot.bin)" \
		} \
	} \
}

$(icedl_firmware_cdl): \
		$(object_sizes_yaml) \
		$(icedl_components_prepared) $(misc_build_dir)/host.dtb $(host_uboot_build_dir)/u-boot.bin \
		rust-dev
	printf %s '$(firmware_json)' | \
		PATH=$(rust_dev_bins):$$PATH \
		PYTHONPATH=$(python_src):$(capdl_src)/python-capdl-tool:$$PYTHONPATH \
			python3 -m icecap_hypervisor.cli firmware -o $(abspath $(dir $@))

$(icedl_firmware_dir)/src/capdl_spec.c: $(icedl_firmware_cdl) $(object_sizes_yaml) $(capdl_tool)
	mkdir -p $(dir $@)
	$(capdl_tool) --code-dynamic-alloc --object-sizes=$(object_sizes_yaml) --code=$@ $<

$(icedl_firmware_dir)/capdl.cpio: $(icedl_firmware_cdl)
	rm -rf $@.links
	cp -rL $(icedl_firmware_dir)/cdl/links $@.links
	(cd $@.links && find . -not -type d | cpio -o --reproducible -H newc > $(abspath $@))

$(icedl_firmware_dir)/capdl.o: $(icedl_firmware_dir)/capdl.cpio
	aarch64-linux-gnu-gcc -c $(c_src)/support/embedded-file.S -o $@ \
		-DSYMBOL=_capdl_archive -DFILE=$< -DSECTION=_archive_cpio

$(capdl_loader): $(icedl_firmware_dir)/src/capdl_spec.c $(icedl_firmware_dir)/capdl.o $(platform_info_h)
	$(MAKE) -f $(c_src)/Makefile CROSS_COMPILE=aarch64-linux-gnu- \
		BUILD=$(c_build_dir)/capdl-loader/build OUT=$(c_build_dir)/capdl-loader/install \
		CFLAGS="-I$(sel4_include_dir) -I$(c_build_dir)/capdl-loader/build/include -I$(compiler_some_libc_include)" \
		LDFLAGS="-T $(abspath $(c_src)/support/root-task-tls.lds") \
		ICECAP_RUNTIME_CONFIG_IN=$(src)/icecap-runtime-root-config.h \
		CAPDL_LOADER_EXTERNAL_SOURCE=$(capdl_src)/capdl-loader-app \
		CAPDL_LOADER_CONFIG_IN_H=/dev/null \
		CAPDL_LOADER_PLATFORM_INFO_H=$(platform_info_h) \
		CAPDL_LOADER_SPEC_SRC=$(icedl_firmware_dir)/src \
		CAPDL_LOADER_CPIO_O=$(icedl_firmware_dir)/capdl.o \
		ROOTS=" \
			$(c_src)/icecap-runtime/icecap.mk \
			$(c_src)/icecap-utils/icecap.mk \
			$(c_src)/icecap-some-libc/icecap.mk \
			$(c_src)/boot/cpio/icecap.mk \
			$(c_src)/boot/capdl-loader-shim/icecap.mk \
			$(c_src)/boot/capdl-loader-core/icecap.mk \
			$(c_src)/boot/capdl-loader/icecap.mk \
			" \
		install

$(app_elf): $(capdl_loader)
	install -D -T $< $@


realm_json = { \
	"plat": "$(icecap_plat)", \
	"num_cores": 1, \
	"realm_id": 0, \
	"hack_realm_affinity": 1, \
	"object_sizes": "$(abspath $(object_sizes_yaml))", \
	"components": { \
		$(call mk_simple_component_entry,fault_handler), \
		$(call mk_simple_component_entry,realm_vmm), \
		"realm_vm": { \
			"dtb": "$(abspath $(misc_build_dir)/realm.dtb)", \
			"kernel": "$(abspath $(realm_linux_build_dir)/$(linux_image_path))", \
			"initrd": "$(abspath $(call buildroot_rootfs_for,realm))", \
			"bootargs": ["earlycon=icecap_vmm", "console=hvc0", "loglevel=7"] \
		} \
	} \
}

$(icedl_realm_cdl): $(call buildroot_rootfs_for,realm) \
		$(icedl_components_prepared) $(misc_build_dir)/realm.dtb $(realm_linux_build_dir)/$(linux_image_path) \
		$(object_sizes_yaml) \
		rust-dev
	printf %s '$(realm_json)' | \
		PATH=$(rust_dev_bins):$$PATH \
		PYTHONPATH=$(python_src):$(capdl_src)/python-capdl-tool:$$PYTHONPATH \
			python3 -m icecap_hypervisor.cli linux-realm -o $(abspath $(dir $@))

$(icedl_realm_dir)/spec.json: $(icedl_realm_cdl) $(object_sizes_yaml) $(capdl_tool)
	$(capdl_tool) --code-dynamic-alloc --object-sizes=$(object_sizes_yaml) --dyndl=$@ $<

$(icedl_realm_dir)/spec.bin: $(icedl_realm_dir)/spec.json $(icedl_realm_cdl) rust-dev
	$(rust_dev_bins)/dyndl-serialize-spec $(icedl_realm_dir)/cdl/links < $< > $@


$(misc_build_dir)/boot.cpio: sel4 $(app_elf)
	mkdir -p $@.links
	cp -rL $(sel4_build_dir)/install/bin/kernel.elf $(sel4_build_dir)/build/kernel.dtb $(app_elf) $@.links
	printf "kernel.elf\nkernel.dtb\napp.elf\n" | cpio -o -D $@.links --reproducible -H newc > $(abspath $@)

$(misc_build_dir)/boot.o: $(misc_build_dir)/boot.cpio
	aarch64-linux-gnu-gcc -c -x assembler-with-cpp $(c_src)/support/embedded-file.S -o $@ \
		-DSYMBOL=_archive_start -DFILE=$< -DSECTION=_archive_cpio


elfloader_extra_cpp_flags := \
	-I$(abspath $(sel4_include_dir)) \
	-I$(abspath $(c_build_dir)/for-elfloader/install/include)

elfloader_extra_c_flags_link := \
	-L$(abspath $(sel4_lib_dir)) \
	-L$(abspath $(c_build_dir)/for-elfloader/install/lib)

.PHONY: elfloader-configure
elfloader-configure: $(misc_build_dir)/boot.o $(misc_build_dir)/kernel-config.cmake elfloader_c | $(elfloader_build_dir)
	cmake -G Ninja \
		-DCMAKE_TOOLCHAIN_FILE=$(abspath $(sel4_src)/gcc.cmake) \
		-DCROSS_COMPILER_PREFIX=aarch64-linux-gnu- \
		-DHACK_SEL4_SRC=$(abspath $(sel4_src)) \
		-DHACK_KERNEL_CONFIG=$(abspath $(misc_build_dir)/kernel-config.cmake) \
		-C $(abspath $(src)/cmake-config/elfloader.cmake) \
		-S $(abspath $(elfloader_src)) \
		-B $(abspath $(elfloader_build_dir)/build) \
		-DPYTHON3=python3 \
		-DICECAP_HACK_EXTRA_CPP_FLAGS="$(elfloader_extra_cpp_flags)" \
		-DICECAP_HACK_EXTRA_C_FLAGS_LINK="$(elfloader_extra_c_flags_link)" \
		-DICECAP_HACK_CMAKE_HELPERS=$(abspath $(sel4_src)/tools/helpers.cmake) \
		-DICECAP_HACK_CMAKE_INTERNAL=$(abspath $(sel4_src)/tools/internal.cmake) \
		-DICECAP_HACK_CMAKE_TOOL_HELPERS_DIR=$(abspath $(sel4_tools_src)/cmake-tool/helpers) \
		-DICECAP_HACK_KERNEL_TOOLS=$(abspath $(sel4_src)/tools) \
		-DICECAP_HACK_KERNEL_DTB=$(abspath $(sel4_build_dir)/build/kernel.dtb) \
		-DICECAP_HACK_ARCHIVE_O=$(abspath $(misc_build_dir)/boot.o) \
		-Dplatform_yaml=$(abspath $(sel4_build_dir)/build/gen_headers/plat/machine/platform_gen.yaml)

.PHONY: elfloader-build
elfloader-build: elfloader-configure
	ninja -C $(abspath $(elfloader_build_dir)/build) elfloader

$(elfloader_build_dir)/build/elfloader: elfloader-build

.PHONY: elfloader
elfloader: $(elfloader_build_dir)/build/elfloader


$(misc_build_dir)/%.script.uimg: $(src)/$(icecap_plat)/%.script.txt | $(misc_build_dir)
	mkimage -n script -T script -d $< -C none $@
