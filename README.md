# Reference ad-hoc build for IceCap

Upstream IceCap development, testing, and CI use a Nix-based build system. However, care has been taken to ensure that the IceCap source code itself is build-system agnostic. Downstream projects may opt to create an "ad-hoc" build for a particular configuration of IceCap.

This repository demonstrates such a build. The build in this repository is accomplished without Nix. This repository exists for the sole purpose of documentation. We strongly recommend that you do not attempt to use this build system for development on IceCap.

If you have any question about this repository, please feel free to reach
out to project lead [Nick Spinale &lt;nick.spinale@arm.com&gt;](mailto:nick.spinale@arm.com).


## Build instructions

Building and running this demo requires Make and Docker.

First, clone this respository and its submodules:

```
git clone --recursive https://gitlab.com/arm-research/security/icecap/reference-ad-hoc-build
cd reference-ad-hoc-build
```

Next, build, run, and enter a Docker container for development:

```
make -C docker run && make -C docker exec
```

Finally, build and run a demo emulated by QEMU (`-M virt`) where a host virtual
machine spawns a confidential virtual machine called a realm, and then
communicates with it via the virtual network:

```
   [container] make
   [container] make run

               # ... wait for the host VM to boot to a shell ...

               # Spawn a VM in a realm:

 [icecap host] icecap-host create 0 /mnt/spec.bin && taskset 0x2 icecap-host run 0 0

               # ... wait for the realm VM to boot to a shell ...
               
               # Type '<enter>@?<enter>' for console multiplexer help.
               # The host VM uses virtual console 0, and the realm VM uses virtual console 1.
               # Switch to the realm VM virtual console by typing '<enter>@1<enter>'.
               # Access the internet from within the real VM via the host VM:

[icecap realm] curl http://example.com

               # Switch back to the host VM virtual console by typing '<enter>@0<enter>'.
               # Interrupt the realm's execution with '<ctrl>-c' and then destroy it:

 [icecap host] icecap-host destroy 0
 
               # '<ctrl>-a x' quits QEMU
```

#### Raspberry Pi 4

To instead build for the Raspberry Pi 4, run the following from within the container:

```
[container] make PLAT=rpi4
```

You will find the contents for a boot partition in `build/rpi4/disposable/misc/boot`.

For more detailed information about running IceCap on the Raspberry Pi 4, please see the README in the primary IceCap repository.
